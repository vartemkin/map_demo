import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    map: {},
    menu: true,
    adding: false,
    markerCounter: 100,
    markers: [
      //   {
      //     id: 1,
      //     lat: 57.01,
      //     lng: 37.01,
      //     selected: false,
      //   },
      //   {
      //     id: 2,
      //     lat: 57.02,
      //     lng: 37.02,
      //     selected: false,
      //   },
      //   {
      //     id: 3,
      //     lat: 57.03,
      //     lng: 37.03,
      //     selected: false,
      //   },
    ],
  },
  mutations: {
    setMap (state, map) {
      state.map = map
    },
    setMarkerCounter (state, value) {
      state.markerCounter = value
    },
    setMarkers (state, markers) {
      state.markers = markers
    },
    toggleMenu (state) {
      state.menu = !state.menu
    },
    toggleAdding (state) {
      state.adding = !state.adding
    },
    addMarker (state, playload) {
      playload.id = state.markerCounter++ // can be get from database
      state.markers.push(playload)
    },
    selectMarker (state, playload) {
      state.markers.forEach((marker) => {
        marker.selected = false
      })
      const found = state.markers.find((marker) => parseInt(marker.id) === parseInt(playload))
      if (found) {
        // console.log(state.markers)
        found.selected = true
      }
    },
    deleteMarker (state, playload) {
      const found = state.markers.find((marker) => marker.id === playload)
      if (found) {
        if (found.removeFromMap) {
          found.removeFromMap()
        }
        const index = state.markers.indexOf(found)
        if (index > -1) {
          state.markers.splice(index, 1)
        }
      }
    },
  },
  actions: {
    invalidateSize ({ state }) {
      state.map.invalidateSize()
    },
    loadMarkers ({ commit }) {
      const data = JSON.parse(localStorage.getItem('markers'))
      if (data !== null) {
        commit('setMarkerCounter', data.markerCounter)
        commit('setMarkers', data.markers)
      }
    },
    async saveMarkers ({ state }) {
      const data = {
        markerCounter: state.markerCounter,
        markers: state.markers.map((marker) => ({
          ...marker,
          instance: false,
        })),
      }
      localStorage.setItem('markers', JSON.stringify(data))

      // you can add debounce()
      try {
        console.log('save markers to uncreated backend')
        await axios.post('/api/v1/markers', data)
      } catch (e) {
        console.log(e)
      }
    },
  },
  modules: {}
})
