import { shallowMount } from '@vue/test-utils'
import MarkerItem from '@/components/MarkerItem.vue'

const $t = (key) => key

describe('MarkerItem.vue', () => {
  it('renders props.marker id when passed', () => {
    const marker = {
      id: 157,
      lat: 50,
      lng: 30,
      selected: false,
    }
    const wrapper = shallowMount(MarkerItem, {
      propsData: { marker },
      mocks: { $t }
    })
    expect(wrapper.text()).toContain(marker.id)
  })
})
